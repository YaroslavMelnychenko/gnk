<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'name',
        'tel',
        'region',
        'settlement',
        'address',
        'postcode',
        'novaposhta',
        'color',
        'price',
        'hash'
    ];
}
