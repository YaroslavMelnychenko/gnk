<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Http\Controllers\LiqPay;
use Telegram\Bot\Api;
use Validator;

class PurchaseController extends Controller
{
    protected $token = "726116088:AAHE7CzAXpduLy-S6CRZNPehrWtQP-ceOW4";
    protected $chatId = "355415752";
    protected $Max = "386823831";
    protected $public_key = "i74969253358";
    protected $private_key = "44TQxnwpkjKfRc6C99BC3Ty2mG51BomDDSsNIjqd";

    protected function sendMessage($msg) {
        $telegram = new Api(config('telegram.bot_token'));

        $message = $telegram->sendMessage(array(
            'chat_id' => $this->chatId,
            'text' => $msg,
            'parse_mode' => 'html'
        ));

        $telegram->forwardMessage(array(
            'chat_id' => $this->Max,
            'from_chat_id' => $this->chatId,
            'message_id' => $message->getMessageId()
        ));
    }

    public function showPurchase() {
        return view('purchase', [
            'name' => 'G&K | Купить'
        ]);
    }

    public function showSuccessful() {
        return view('successful', [
            'name' => 'G&K | Успешная покупка'
        ]);
    }

    public function showFailed() {
        return view('failed', [
            'name' => 'G&K | Оплата не удалась'
        ]);
    }

    public function showAgreement() {
        return view('agreement', [
            'name' => "G&K | Пользовательское соглашение"
        ]);
    }

    protected function createOrder($request) {
        if($order = Order::create([
            'name' => $request->name,
            'tel' => $request->tel,
            'region' => $request->region,
            'settlement' => $request->settlement,
            'address' => $request->address,
            'postcode' => $request->postcode,
            'novaposhta' => $request->novaposhta,
            'color' => $request->color,
            'price' => $request->price
        ])) {
            $id = $order->id;
            $hash = sha1($id.$request->name.$request->tel);
            $order->hash = $hash;
            $order->save();
            return $order;
        } else {
            return false;
        }
    }

    protected function generatePayLink($order) {
        $options = array(
            'public_key' => $this->public_key,
            'version' => '3',
            'action' => 'pay',
            'amount' => $order->price,
            'currency' => 'UAH',
            'description' => 'GENTLEMAN`S BOX ('.$order->color.')',
            'order_id' => $order->hash
        );
      
        $data = base64_encode(json_encode($options));
        $signature = base64_encode(sha1($this->private_key.$data.$this->private_key, 1));

        return json_encode(array(
            'data' => $data,
            'signature' => $signature
        ));
    }

    public function addNew(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'tel' => 'required|max:25',
            'region' => 'max:255',
            'settlement' => 'required|max:255',
            'address' => 'required|max:255',
            'postcode' => 'max:5',
            'novaposhta' => 'max:10',
            'color' => 'required',
            'price' => 'required'
        ]);
        if($validator->fails()) {
            return response('validation error', 200);
        } else {
            if(!($order = $this->createOrder($request))) {
                return response('creation error', 200);
            } else {
                $payment = $this->generatePayLink($order);

                if($request->region == '') $region = 'не указано';
                else $region = $request->region;

                if($request->postcode == '') $postcode = 'не указано';
                else $postcode = $request->postcode;

                if($request->novaposhta == '') $novaposhta = 'не указано';
                else $novaposhta = $request->novaposhta;

                $msg =  "<b>Новый заказ</b>\n"
                        ."<b>Имя</b>: $request->name\n"
                        ."<b>Номер телефона</b>: $request->tel\n"
                        ."<b>Область</b>: $region\n"
                        ."<b>Населённый пункт</b>: $request->settlement\n"
                        ."<b>Адрес</b>: $request->address\n"
                        ."<b>Почтовый индекс</b>: $postcode\n"
                        ."<b>Отделение Новой Почты</b>: $novaposhta\n"
                        ."<b>Цвет</b>: $request->color\n"
                        ."<b>Цена</b>: $request->price грн\n"
                        ."<b>Код заказа</b>: $order->hash";

                $this->sendMessage($msg);

                return response($payment, 200);
            }
        }
    }
}
