<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@showHome')->name('home.show');
Route::get('/about', 'AboutController@showAbout')->name('about.show');
Route::get('/purchase', 'PurchaseController@showPurchase')->name('purchase.show');
Route::get('/purchase/successful', 'PurchaseController@showSuccessful')->name('purchase.successful');
Route::get('/purchase/failed', 'PurchaseController@showFailed')->name('purchase.failed');
Route::any('/purchase/new', 'PurchaseController@addNew')->name('purchase.add');
Route::post('/purchase/callback', 'PurchaseController@paymentCallback')->name('purchase.callback');
Route::get('/user-agreement', 'PurchaseController@showAgreement')->name('purchase.agreement');
