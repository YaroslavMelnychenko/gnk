window.Menu = {
    isOpen: false,

    open: function(f) {
        if(!this.isOpen) {
            $('.menu').toggleClass('shown');
            $('.overlay').toggleClass('shown');
            $('.section').toggleClass('overlayed');
            if(f){
                fullpage_api.setAllowScrolling(false);
                fullpage_api.setKeyboardScrolling(false);
            }
            this.isOpen = true;
        }
    },

    close: function(f) {
        if(this.isOpen) {
            $('.menu').toggleClass('shown');
            $('.overlay').toggleClass('shown');
            $('.section').toggleClass('overlayed');
            if(f){
                fullpage_api.setAllowScrolling(true);
                fullpage_api.setKeyboardScrolling(true);
            }
            this.isOpen = false;
        }
    }
}

import Typed from 'typed.js';
import Axios from 'axios';
import Vue from 'vue';
import swal from 'sweetalert';

$(document).ready(function() {
    if(checkElement('.sections') && window.innerWidth >= 1280) {
        new fullpage('.sections', {
            licenseKey: '1sjlIO!oss',
            verticalCentered: false,
            anchors: ['home', 'product', 'slider', 'about', 'contacts'],
            scrollOverflow: true,
            animateAnchor: false,
            menu: '#sections_menu',
            easingcss3: 'ease-in-out',
            loopBottom: true,
            normalScrollElements: '.menu',
            onLeave: function(origin, destination, direction) {
                var leavingSection = this;

                if(origin.index == 0 && direction == 'down') {
                    $('.product-column.right').addClass('to-left');
                } else if(origin.index == 1 && direction == 'up') {
                    $('.product-column.right').removeClass('to-left');
                }
            }
        });
    }
    
    if(checkElement('.abouts') && window.innerWidth >= 1280) {
        new fullpage('.abouts', {
            licenseKey: '1sjlIO!oss',
            verticalCentered: false,
            anchors: ['gift-box', 'necktie', 'clamp', 'shawl', 'cufflinks', 'letter'],
            scrollOverflow: true,
            animateAnchor: false,
            easingcss3: 'ease-in-out',
            normalScrollElements: '.menu'
        });
    }    

    $('.home-bars').click(function() {
        if((checkElement('.sections') || checkElement('.abouts')) && window.innerWidth >= 1280)  {
            Menu.open(true);
        } else {
            Menu.open(false);
        }
    });

    $('.menu-cross').click(function() {
        if((checkElement('.sections') || checkElement('.abouts')) && window.innerWidth >= 1280) {
            Menu.close(true);
        } else {
            Menu.close(false);
        }
    });

    $('.overlay').click(function() {
        if((checkElement('.sections') || checkElement('.abouts')) && window.innerWidth >= 1280) {
            Menu.close(true);
        } else {
            Menu.close(false);
        }
    });

    $('.menu-link').click(function() {
        if((checkElement('.sections') || checkElement('.abouts')) && window.innerWidth >= 1280) {
            Menu.close(true);
        } else {
            Menu.close(false);
        }
    });

    $('.top-mouse, .top-left-more').click(function() {
        fullpage_api.moveSectionDown();
    });

    if(checkElement('.slider-slick')) {
        $('.slider-slick').slick({
            dots: true,
            prevArrow:
            '<button class="slick-prev slick-arrow" aria-label="Previous" type="button"><span></span></button>',
            nextArrow:
            '<button class="slick-next slick-arrow" aria-label="Next" type="button"><span></span></button>',
            autoplay: true,
            autoplaySpeed: 5000,
            pauseOnFocus: false,
            pauseOnHover: false
        });
    }

    if(checkElement('#about_quotes')){
        var typed = new Typed('#about_quotes', {
            strings: ['G&amp;K - это больше чем подарок.', 'Мы дарим настоящие эмоции.', 'Начни новый год с выбора правильного подарка.'],
            typeSpeed: 50,
            backSpeed: 40,
            fadeOut: true,
            loop: true,
            backDelay: 3000
        });
    }

    $('.about-right .read-more').click(function() {
        var data = $(this).attr('data-more');
        $(this).toggleClass('active');
        $('.about-right .opacied[data-more="' + data + '"]').toggleClass('active');
        $('.about-right .table[data-more="' + data + '"]').toggleClass('active');
    });

    $('.open-letter').click(function() {
        $('.letter-text').toggleClass('active');
        $('.letter-notice').toggle(250);
    });

    $('#purchase_form').submit(function(e){
        e.preventDefault();
        var color = $('[name="color"]:checked').val();
        var letter = $('[name="letter"]').val();
        if(letter.length == 0){
            letter = "пусто";
        }
        if(color == 'sapfir') {
            color = 'сапфир';
        } 
        if(color == 'grafit') {
            color = 'графит';
        }
        var tel = $('[name="tel"]').val();
        var region = $('[name="region"]').val();
        var locality = $('[name="locality"]').val();
        var address = $('[name="address"]').val();
        var index = $('[name="index"]').val();
        var office = $('[name="office"]').val();
        var fullAddress = "Телефон: " + tel + ", Область: " + region + ", населенный пункт: " + locality + ", адрес: " + address + ", индекс: " + index + ", отделение новой почты: " + office;
        Axios.get('/purchase/new?color=' + color + '&letter=' + letter + '&address=' + fullAddress).then(response => {
            if(response.status == "200"){
                $('#liqpay_form input[name="data"]').val(response.data.data);
                $('#liqpay_form input[name="signature"]').val(response.data.signature);
                $('#liqpay_form').submit();
            }
            //$('#liqpay_form input[name="data"]').val(response.data.data);
            //$('#liqpay_form').submit();
        });
    });

    $('.color').click(function(){
        if(!$(this).hasClass('selected')){
            var color = $(this).attr('data-color');
            var section = $(this).attr('data-change');
            $('.color[data-change="'+section+'"]').removeClass('selected');
            $(this).addClass('selected');
            if(section == "necktie") {
                if(color == "blue") {
                    $('.section-2').removeClass('black');
                    $('.section-2').addClass('blue');
                    $('.necktie .table').removeClass('black');
                    $('.necktie .table').addClass('blue');
                } 
                if(color == "black") {
                    $('.section-2').removeClass('blue');
                    $('.section-2').addClass('black');
                    $('.necktie .table').removeClass('blue');
                    $('.necktie .table').addClass('black');
                }
            }
            if(section == "shawl") {
                if(color == "blue") {
                    $('.section-4').removeClass('black');
                    $('.section-4').addClass('blue');
                    $('.shawl .table').removeClass('black');
                    $('.shawl .table').addClass('blue');
                } 
                if(color == "black") {
                    $('.section-4').removeClass('blue');
                    $('.section-4').addClass('black');
                    $('.shawl .table').removeClass('blue');
                    $('.shawl .table').addClass('black');
                }
            }
        }
    });

    $('.menu-link').click(function(e){
        if(window.innerWidth < 1280) {
            e.preventDefault();
            var href = $(this).attr('href');
            location.href = href + '-anchor';
            $('.menu-list li').removeClass('active');
            $(this).parent('li').addClass('active');
        }
    });

    if(window.innerWidth < 1280){
        window.scrollTo(0, 0);
    }

    if(checkElement('#vue-purchase-form')) {
        window.purchaseForm = new Vue({
            el: '#vue-purchase-form'
        });
    }
});

$(window).keyup(function(e){
    var evtobj = window.event ? event : e;
    if (evtobj.keyCode == 37) {
        Menu.open();
    } else if(evtobj.keyCode == 39) {
        Menu.close();
    }
});

if(checkElement('#vue-letter')) {
    var letter = new Vue({
        el: '#vue-letter',
        data: {
            letterText: ''
        }
    });
}
