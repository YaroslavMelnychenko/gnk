@extends('layouts.master')

@section('content')
    <div class="home">
        <div class="sections">
            <section id="home-anchor" class="top section" data-anchor="home">
                <div class="content">
                    @component('components.home.logo')
                        on-top
                    @endcomponent
                    @component('components.home.bars')
                        on-top
                    @endcomponent
                    @component('components.home.socials')
                        on-top
                    @endcomponent
                    <div class="top-mouse"></div>
                    <div class="top-row">
                        <div class="top-left">
                            <div class="top-left-sign-block">
                                <div class="top-left-play"></div>
                                <div class="top-left-sign"></div>
                                <div class="top-left-text">Истинная эмоция для<br>любого мужчины.</div>
                                <div class="top-left-more-wrapper">
                                    <a href="#" class="top-left-more">Подробнее</a>
                                </div>
                            </div>
                        </div>
                        <div class="top-right">
                            <div class="top-right-title">
                                <div class="top-right-cart"></div>
                                <h2 class="top-right-text">gentleman’s box</h2>
                                <a href="/about" class="top-right-more more-link">Узнать больше</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="product-anchor" class="product section" data-anchor="product">
                <div class="content">
                    @component('components.home.logo')
                        on-product
                    @endcomponent
                    @component('components.home.bars')
                        on-slider
                    @endcomponent
                    @component('components.home.socials')
                        on-product
                    @endcomponent
                    <div class="remain">
                        <div class="title">Осталось</div>
                        <div class="count">
                            <span>92</span>
                            <span>/</span>
                            <span>100</span>
                        </div>
                    </div>
                    <div class="product-row">
                        <div class="product-column left">
                            <h3 class="title">Мы поможем тебе выбрать идеальный подарок.</h3>
                            <p class="text">Ты больше не знаешь что подарить? Ты устала ходить по магазинам в поисках нужной вещи? Тебя раздражают некачественные вещи? Тогда тебе сюда. Мы создаем нечто большее, чем подарок. Мы создаем — элегантность, роскошь и стиль жизни. Твой близкий человек — будет в восторге. Ты готова удивить его?</p>
                            <div class="box-content">
                                <h4 class="box-content-title">Набор состоит из:</h4>
                                <div class="box-content-row">
                                    <a href="/about#necktie" class="box-content-column">
                                        <div class="box-content-thumbnail first"></div>
                                        <div class="box-content-notice">Галстук</div>
                                    </a>
                                    <a href="/about#clamp" class="box-content-column">
                                        <div class="box-content-thumbnail second"></div>
                                        <div class="box-content-notice">Зажим</div>
                                    </a>
                                    <a href="/about#shawl" class="box-content-column">
                                        <div class="box-content-thumbnail third"></div>
                                        <div class="box-content-notice">Платок</div>
                                    </a>
                                    <a href="/about#cufflinks" class="box-content-column">
                                        <div class="box-content-thumbnail fourth"></div>
                                        <div class="box-content-notice">Запонки</div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="product-column right">
                            <div class="purchase-wrapper">
                                <a href="/about" class="purchase-more more-link">Узнать больше</a>
                                <h2 class="purchase-title">gentleman’s box</h2>
                                <a href="/purchase" class="purchase-button">Купить</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="slider section" data-anchor="slider">
                <div class="content">
                    @component('components.home.logo')
                        on-slider
                    @endcomponent
                    @component('components.home.bars')
                        on-slider
                    @endcomponent
                    @component('components.home.socials')
                        on-slider
                    @endcomponent
                    @component('components.home.order')
                        on-slider
                    @endcomponent
                    <div class="slider-container">
                        <div class="slider-slick">
                            <div class="slider-slick-item item-1"></div>
                            <div class="slider-slick-item item-2"></div>
                            <div class="slider-slick-item item-3"></div>
                            <div class="slider-slick-item item-4"></div>
                        </div>
                    </div> 
                </div>
            </section>
            <section id="about-anchor" class="about section" data-anchor="about">
                <div class="content">
                    @component('components.home.logo')
                        on-about
                    @endcomponent
                    @component('components.home.bars')
                        on-about
                    @endcomponent
                    @component('components.home.socials')
                        on-about
                    @endcomponent
                    @component('components.home.order')
                        on-about
                    @endcomponent
                    <div class="rectangle">
                        <h2 id="about_quotes"></h2>
                    </div>
                    <div class="about-quote">
                        <div class="emblem"></div>
                        <div class="quote">by the people, for the people</div>
                    </div>
                </div>
            </section>
            <section id="contacts-anchor" class="contacts section" data-anchor="contacts">
                <div class="content">
                    @component('components.home.logo')
                        on-contacts
                    @endcomponent
                    @component('components.home.bars')
                        on-contacts
                    @endcomponent
                    @component('components.home.order')
                        on-contacts
                    @endcomponent
                    <div class="made-by">
                        Made by: <a href="http://pinselstudio.com">Pinsel.Studio</a>
                    </div>
                    <div class="copyright">
                        Copyright © 2018 <span>G&K</span>. All rights reserved.
                    </div>
                    <div class="contacts-wrapper">
                        <div class="contacts-row">
                            <div class="contacts-left">
                                <div class="first">
                                    <div class="socials">
                                        <a href="#" target="_blank">
                                            <svg aria-hidden="true" data-prefix="fab" data-icon="facebook-f" class="svg-inline--fa fa-facebook-f fa-w-9" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 264 512"><path fill="currentColor" d="M76.7 512V283H0v-91h76.7v-71.7C76.7 42.4 124.3 0 193.8 0c33.3 0 61.9 2.5 70.2 3.6V85h-48.2c-37.8 0-45.1 18-45.1 44.3V192H256l-11.7 91h-73.6v229"></path></svg>
                                        </a>
                                        <a href="https://www.instagram.com/gk.genre/" target="_blank">
                                            <svg aria-hidden="true" data-prefix="fab" data-icon="instagram" class="svg-inline--fa fa-instagram fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z"></path></svg>
                                        </a>
                                    </div>
                                </div>
                                <div class="second">
                                    <div class="title">gentleman’s box</div>
                                    <div class="thumbnail-wrapper">
                                        <div class="thumbnail"></div>
                                    </div>
                                    <div class="more-wrapper">
                                        <a href="/about" class="more more-link">Узнать больше</a>
                                    </div>
                                </div>
                                <div class="third">
                                    <a href="/purchase" class="cart"></a>
                                </div>
                            </div>
                            <div class="contacts-right">
                                <div class="contacts-right-inner">
                                    <div class="title">Контакты:</div>
                                    <div class="text-block">
                                        <strong>Киев, ул. Васильковская 37</strong>
                                        <small>(главный офис)</small>
                                    </div>
                                    <div class="text-block">
                                        <strong>+ 380 68 807 40 69</strong>
                                        <small>(контактный номер телефона)</small>
                                        <div class="icons">
                                            <a href="tg://resolve?domain=WizzardTheCaptain" class="icon telegram"></a>
                                            <a href="viber://chat?number=+380688074069" class="icon viber"></a>
                                        </div>
                                    </div>
                                    <div class="text-block">
                                        <strong>gorshkovkucherenko@gmail.com</strong>
                                        <small>(контактный e-mail)</small>
                                        <div class="icons">
                                            <a href="mailto:gorshkovkucherenko@gmail.com" class="icon alone mail"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        @component('components.menu')
            anchors
        @endcomponent
        <div class="overlay"></div>
    </div>
@endsection