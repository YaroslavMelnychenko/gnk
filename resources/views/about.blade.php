@extends('layouts.master')

@section('content')
    <div class="home">
        <div class="abouts">
            <section class="gift-box section fp-auto-height" data-anchor="gift-box">
                <div class="content">
                    @component('components.home.logo')
                        on-top
                    @endcomponent
                    @component('components.home.bars')
                        on-top
                    @endcomponent
                    @component('components.home.socials')
                        on-top
                    @endcomponent
                    @component('components.about.side')
                        @slot('title')
                            gift-box
                        @endslot
                        black
                    @endcomponent
                    <div class="about-row">
                        <div class="about-left section-1">
                            @component('components.about.present')
                                black
                            @endcomponent
                        </div>
                        <div class="about-right">
                            <h2 class="title">gentleman’s box</h2>
                            <div class="text">Деревянная коробка G&K изготовляется из качественных материалов. Внутри представлены: галстук, платок, запонки и зажим. Также внутри каждой коробки есть конверт с печатью и карточкой, на которой мы напишем пожелания вашему близкому человеку. Мы гарантируем ВЫСОКОЕ качество нашей продукции и несем за это ответственность. Доставка осуществляется бесплатно по всей Украине Новой Почтой или Укрпочтой (курьером по Киеву).</div>
                            <div class="who-give-wrapper">
                                <div class="who-give-title">Кому Подарить?</div>
                                <div class="who-give-block">
                                    <div class="who-give-element">
                                        <span>Руководителю</span>
                                    </div>
                                    <div class="who-give-element">
                                        <span>Бизнес партнёру</span>
                                    </div>
                                </div>
                                <div class="who-give-block">
                                    <div class="who-give-element">
                                        <span>Папе\Брату</span>
                                    </div>
                                    <div class="who-give-element">
                                        <span>Мужу\Парню</span>
                                    </div>
                                </div>
                            </div>
                            <div class="box-content">
                                <h4 class="box-content-title">Наполнение:</h4>
                                <div class="box-content-row">
                                    <div class="box-content-column">
                                        <div class="box-content-thumbnail first"></div>
                                        <div class="box-content-notice">Галстук</div>
                                    </div>
                                    <div class="box-content-column">
                                        <div class="box-content-thumbnail second"></div>
                                        <div class="box-content-notice">Зажим</div>
                                    </div>
                                    <div class="box-content-column">
                                        <div class="box-content-thumbnail third"></div>
                                        <div class="box-content-notice">Платок</div>
                                    </div>
                                    <div class="box-content-column">
                                        <div class="box-content-thumbnail fourth"></div>
                                        <div class="box-content-notice">Запонки</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section> 
            <section class="necktie section fp-auto-height" data-anchor="necktie">
                <div class="content">
                    @component('components.home.logo')
                        on-top
                    @endcomponent
                    @component('components.home.bars')
                        on-top
                    @endcomponent
                    @component('components.home.socials')
                        on-slider
                    @endcomponent
                    @component('components.about.side')
                        @slot('title')
                            necktie
                        @endslot
                        black
                    @endcomponent
                    <div class="about-row">
                        <div class="about-left section-2 blue">
                            @component('components.about.present')
                                white
                            @endcomponent
                        </div>
                        <div class="about-right">
                            <div class="read-more" data-more="necktie">
                                <div class="plus-in-circle">
                                    <span></span>
                                    <span></span>
                                </div>
                                <div class="text">Подробнее</div>
                            </div>
                            <div class="table blue" data-more="necktie"></div>
                            <div>
                                <div class="title">
                                    Галстук
                                </div>
                                <div class="colors">
                                    <div class="color selected" data-change="necktie" data-color="blue"></div>
                                    <div class="color" data-change="necktie" data-color="black"></div>
                                </div>
                            </div>
                            <div class="text opacied" data-more="necktie">Галстук изготовлен из качественного шелка с шерстью. Есть два цвета на выбор: черный и темно-синий. В зависимости от вкусовых предпочтений вашего близкого человека. Мы позаботились о вас и сделали галстуки исключительно черного и синего цветов. Так как галстуки темных тонов используются для ежедневной носки. Они надеваются, как правило, со светлыми рубашками.</div>
                            <div class="quality-block opacied" data-more="necktie">
                                <div class="quality-block-title">Мы в ответе за качество:</div>
                                <div class="icons-row">
                                    <div class="icon-block">
                                        <div class="icon palm"></div>
                                        <p>100%<br><span>Handmade</span></p>
                                    </div>
                                    <div class="icon-block">
                                        <div class="icon spool"></div>
                                        <p>Свое<br><span>производство</span></p>
                                    </div>
                                    <div class="icon-block">
                                        <div class="icon eco"></div>
                                        <p>Эко<br><span>материалы</span></p>
                                    </div>
                                    <div class="icon-block">
                                        <div class="icon heart"></div>
                                        <p>С<br><span>любовью</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section> 
            <section class="clamp section" data-anchor="clamp">
                <div class="content">
                    @component('components.home.logo')
                        on-top
                    @endcomponent
                    @component('components.home.bars')
                        on-top
                    @endcomponent
                    @component('components.home.socials')
                        black
                    @endcomponent
                    @component('components.about.side')
                        @slot('title')
                            clamp
                        @endslot
                        white-before
                    @endcomponent
                    <div class="about-row">
                        <div class="about-left section-3">
                            @component('components.about.present')
                                white
                            @endcomponent
                        </div>
                        <div class="about-right">
                            <div class="read-more" data-more="clamp">
                                <div class="plus-in-circle">
                                    <span></span>
                                    <span></span>
                                </div>
                                <div class="text">Подробнее</div>
                            </div>
                            <div class="table" data-more="clamp"></div>
                            <div class="title">Зажим</div>
                            <div class="text opacied" data-more="clamp">Зажим изготовлен из Латуни. Специальный качественный материал, который имеет абсолютные преимущества. И самое главное не ржавеет. Зажим для галстука должен гармонировать с цветовой гаммой костюма и запонками. Зажим должен соответствовать ширине галстука (составлять от 1/2 до 3/4 ширины галстука и ни в коем случае не быть шире галстука).</div>
                            <div class="quality-block opacied" data-more="clamp">
                                <div class="quality-block-title">Мы в ответе за качество:</div>
                                <div class="icons-row">
                                    <div class="icon-block">
                                        <div class="icon thumb"></div>
                                        <p>Безупречное<br><span>качество продукта</span></p>
                                    </div>
                                    <div class="icon-block">
                                        <div class="icon factory"></div>
                                        <p>Свое<br><span>производство</span></p>
                                    </div>
                                    <div class="icon-block">
                                        <div class="icon eco"></div>
                                        <p>Эко<br><span>материалы</span></p>
                                    </div>
                                    <div class="icon-block">
                                        <div class="icon heart"></div>
                                        <p>С<br><span>любовью</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section> 
            <section class="shawl section" data-anchor="shawl">
                <div class="content">
                    @component('components.home.logo')
                        on-top
                    @endcomponent
                    @component('components.home.bars')
                        on-top
                    @endcomponent
                    @component('components.home.socials')
                        on-slider
                    @endcomponent
                    @component('components.about.side')
                        @slot('title')
                            shawl
                        @endslot
                        black
                    @endcomponent
                    <div class="about-row">
                        <div class="about-left section-4 blue">
                            @component('components.about.present')
                                white
                            @endcomponent
                        </div>
                        <div class="about-right">
                            <div class="read-more" data-more="shawl">
                                <div class="plus-in-circle">
                                    <span></span>
                                    <span></span>
                                </div>
                                <div class="text">Подробнее</div>
                            </div>
                            <div class="table blue" data-more="shawl"></div>
                            <div>
                                <div class="title">Платок</div>
                                <div class="colors">
                                    <div class="color selected" data-change="shawl" data-color="blue"></div>
                                    <div class="color" data-change="shawl" data-color="black"></div>
                                </div>
                            </div>
                            <div class="text opacied" data-more="shawl">Платок изготовлен из качественного шелка с шерстью. Подбирается исключительно под цвет галстука. Есть два цвета: черный и темно-синий. Платок является одним из ключевых элементов мужского стиля. Красиво подчеркивает элегантность и мужественность характера.</div>
                            <div class="quality-block opacied" data-more="shawl">
                                <div class="quality-block-title">Мы в ответе за качество:</div>
                                <div class="icons-row">
                                    <div class="icon-block">
                                        <div class="icon thumb"></div>
                                        <p>Безупречное<br><span>качество продукта</span></p>
                                    </div>
                                    <div class="icon-block">
                                        <div class="icon yarn"></div>
                                        <p>Свое<br><span>производство</span></p>
                                    </div>
                                    <div class="icon-block">
                                        <div class="icon eco"></div>
                                        <p>Эко<br><span>материалы</span></p>
                                    </div>
                                    <div class="icon-block">
                                        <div class="icon heart"></div>
                                        <p>С<br><span>любовью</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section> 
            <section class="cufflinks section" data-anchor="cufflinks">
                <div class="content">
                    @component('components.home.logo')
                        on-slider
                    @endcomponent
                    @component('components.home.bars')
                        on-top
                    @endcomponent
                    @component('components.home.socials')
                        on-slider
                    @endcomponent
                    @component('components.about.side')
                        @slot('title')
                            cufflinks
                        @endslot
                        black
                    @endcomponent
                    <div class="about-row">
                        <div class="about-left section-5">
                            @component('components.about.present')
                                white
                            @endcomponent
                        </div>
                        <div class="about-right">
                            <div class="read-more" data-more="cufflinks">
                                <div class="plus-in-circle">
                                    <span></span>
                                    <span></span>
                                </div>
                                <div class="text">Подробнее</div>
                            </div>
                            <div class="table" data-more="cufflinks"></div>
                            <div class="title">Запонки</div>
                            <div class="text opacied" data-more="cufflinks">Запонки изготовлены из латуни. Внутри черная эмаль. Запонки являются атрибутом стиля. Любой уважающий себя мужчина это подтвердит. Запонки входят в число официальных аксессуаров для костюма и именно они позволяют выглядеть достойно в любой ситуации.</div>
                            <div class="quality-block opacied" data-more="cufflinks">
                                <div class="quality-block-title">Мы в ответе за качество:</div>
                                <div class="icons-row">
                                    <div class="icon-block">
                                        <div class="icon thumb"></div>
                                        <p>Безупречное<br><span>качество продукта</span></p>
                                    </div>
                                    <div class="icon-block">
                                        <div class="icon factory"></div>
                                        <p>Свое<br><span>производство</span></p>
                                    </div>
                                    <div class="icon-block">
                                        <div class="icon eco"></div>
                                        <p>Эко<br><span>материалы</span></p>
                                    </div>
                                    <div class="icon-block">
                                        <div class="icon heart"></div>
                                        <p>С<br><span>любовью</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>        
            </section> 
            <section id="vue-letter" class="letter section" data-anchor="letter">
                <div class="content">
                    @component('components.home.logo')
                        on-slider
                    @endcomponent
                    @component('components.home.bars')
                        on-top
                    @endcomponent
                    @component('components.home.socials')
                        on-slider
                    @endcomponent   
                    @component('components.about.side')
                        @slot('title')
                            letter
                        @endslot
                        white-before
                    @endcomponent 
                    <div class="about-row">
                        <div class="about-left section-6">
                            @component('components.about.present')
                                white
                            @endcomponent
                            <div class="letter-text-vue">
                                @{{ letterText }}
                            </div>
                        </div>
                        <div class="about-right">
                            <div class="read-more" data-more="letter">
                                <div class="plus-in-circle">
                                    <span></span>
                                    <span></span>
                                </div>
                                <div class="text">Написать письмо</div>
                            </div>
                            <div class="table" data-more="letter">
                                <textarea v-model="letterText" autofocus placeholder="Ваше письмо" name="letter_text" id="letter_text" maxlength="50"></textarea>
                            </div>
                            <div class="title">Письмо</div>
                            <div class="text opacied" data-more="letter">Придумайте своё пожелание, которое мы с радостью напишем на подарочной карточке</div>
                            <div class="quality-block opacied" data-more="letter">
                                <div class="quality-block-title">Мы в ответе за качество:</div>
                                <div class="icons-row">
                                    <div class="icon-block">
                                        <div class="icon letter"></div>
                                        <p>Уникальное<br><span>письмо от вас</span></p>
                                    </div>
                                    <div class="icon-block">
                                        <div class="icon add"></div>
                                        <p>Ваше<br><span>пожелание</span></p>
                                    </div>
                                    <div class="icon-block">
                                        <div class="icon heart"></div>
                                        <p>С<br><span>любовью</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
            </section> 
        </div>
        @component('components.menu')
            links
        @endcomponent
        <div class="overlay"></div>
    </div>
@endsection