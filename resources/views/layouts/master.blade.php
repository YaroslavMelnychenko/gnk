<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta id="viewport" name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $name }}</title>
    <link rel="stylesheet" href="{{ asset('/css/common.min.css') }}">
</head>
<body>
    <div id="preloader" class="frame">
        <div class="center">
            <div class="dot-1"></div>
            <div class="dot-2"></div>
            <div class="dot-3"></div>
        </div>
    </div>
    @yield('content')
    <script>
      window.senderCallback = function() {
        SenderWidget.init({
          companyId: "i315534519"
        });
      }
    </script>
    <script>
      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        js = d.createElement(s);
        js.id = id;
        js.src = "https://widget.sender.mobi/build/init.js";
        fjs.parentNode.insertBefore(js, fjs, 'sender-widget');
      })(document, 'script');
    </script>
    <script src="{{ asset('/js/common.min.js') }}"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-128513148-2"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-128513148-2');
		</script>
</body>
</html>