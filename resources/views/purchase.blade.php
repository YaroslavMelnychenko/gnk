@extends('layouts.master')

@section('content')
    <div class="purchase">
        <div class="purchase-logo-row">
            <div class="purchase-logo-visa-mastercard"></div>
            <div class="purchase-logo"></div>
            <a href="http://liqpay.ua" target="_blank" class="purchase-logo-liqpay"></a>
        </div>
        <div id="vue-purchase-form" class="purchase-form-wrapper">
            <purchase-form></purchase-form>
        </div>
    </div>
@endsection