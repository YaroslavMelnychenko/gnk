@if ($slot == 'links')
<div class="menu">
    <div class="menu-book-cross">
        <div class="menu-cross-wrapper">
            <div class="menu-cross">
                <span></span>
                <span></span>
            </div>
        </div>
        <div class="menu-book">
            <a href="/purchase" class="menu-book-title">Купить набор</a>
            <div class="menu-book-count-wrapper">
                <div class="menu-book-remain">Осталось</div>
                <div class="menu-book-count">
                    <span class="left">82</span>
                    <span>/</span>
                    <span class="all">100</span>
                </div>
            </div>
        </div>
    </div>
    <ul class="menu-list">
        <li>
            <a class="menu-link" href="/#home">Главная</a>
        </li>
        <li>
            <a class="menu-link" href="/#product">Продукция</a>
        </li>
        <li>
            <a class="menu-link" href="/#about">О нас</a>
        </li>
        <li>
            <a class="menu-link" href="/#contacts">Контакты</a>
        </li>
    </ul>
</div>    
@else
<div class="menu">
    <div class="menu-book-cross">
        <div class="menu-cross-wrapper">
            <div class="menu-cross">
                <span></span>
                <span></span>
            </div>
        </div>
        <div class="menu-book">
            <a href="/purchase" class="menu-book-title">Купить набор</a>
            <div class="menu-book-count-wrapper">
                <div class="menu-book-remain">Осталось</div>
                <div class="menu-book-count">
                    <span class="left">82</span>
                    <span>/</span>
                    <span class="all">100</span>
                </div>
            </div>
        </div>
    </div>
    <ul id="sections_menu" class="menu-list">
        <li class="active" data-menuanchor="home">
            <a class="menu-link" href="#home">Главная</a>
        </li>
        <li data-menuanchor="product">
            <a class="menu-link" href="#product">Продукция</a>
        </li>
        <li data-menuanchor="about">
            <a class="menu-link" href="#about">О нас</a>
        </li>
        <li data-menuanchor="contacts">
            <a class="menu-link" href="#contacts">Контакты</a>
        </li>
    </ul>
</div>
@endif
