<ul id="abouts_menu" class="side-menu {{ $slot }}">
    @if ($title == "gift-box")
        <li class="active" data-menuanchor="gift-box">
            <a class="menu-link" href="#gift-box">Подарочный Набор</a>
        </li>
    @else
        <li data-menuanchor="gift-box">
            <a class="menu-link" href="#gift-box">Подарочный Набор</a>
        </li>
    @endif
    @if ($title == "necktie")
        <li class="active" data-menuanchor="necktie">
            <a class="menu-link" href="#necktie">Галстук</a>
        </li>
    @else
        <li data-menuanchor="necktie">
            <a class="menu-link" href="#necktie">Галстук</a>
        </li>
    @endif
    @if ($title == "clamp")
        <li class="active" data-menuanchor="clamp">
            <a class="menu-link" href="#clamp">Зажим</a>
        </li>
    @else
        <li data-menuanchor="clamp">
            <a class="menu-link" href="#clamp">Зажим</a>
        </li>  
    @endif
    @if ($title == "shawl")
        <li class="active" data-menuanchor="shawl">
            <a class="menu-link" href="#shawl">Платок</a>
        </li>
    @else
        <li data-menuanchor="shawl">
            <a class="menu-link" href="#shawl">Платок</a>
        </li> 
    @endif
    @if ($title == "cufflinks")
        <li class="active" data-menuanchor="cufflinks">
            <a class="menu-link" href="#cufflinks">Запонки</a>
        </li>
    @else
        <li data-menuanchor="cufflinks">
            <a class="menu-link" href="#cufflinks">Запонки</a>
        </li> 
    @endif
    @if ($title == "letter")
        <li class="active" data-menuanchor="letter">
            <a class="menu-link" href="#letter">Письмо</a>
        </li>
    @else
        <li data-menuanchor="letter">
            <a class="menu-link" href="#letter">Письмо</a>
        </li>
    @endif
</ul> 