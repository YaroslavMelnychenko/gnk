var gulp        =   require('gulp'),
    uglify      =   require('gulp-uglify'),
    rename      =   require('gulp-rename'),
    autoprfxr   =   require('gulp-autoprefixer'),
    cssmin      =   require('gulp-clean-css'),
    csscomb     =   require('gulp-csscomb'),
    gcmq        =   require('gulp-group-css-media-queries'),
    smartgrid   =   require('smart-grid'),
    bs          =   require('browser-sync');

var path = {
    js: {
        assembled:  'public/assembled/js/*.js',
        minified:   'public/js/'
    },
    css: {
        assembled:  'public/assembled/css/*.css',
        minified:   'public/css/'
    },
    smartgrid:      'resources/sass'
}

gulp.task('minify:js', () => {
    gulp.src(path.js.assembled)
        .pipe(uglify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest(path.js.minified));
});

gulp.task('minify:css', () => {
    gulp.src(path.css.assembled)
        .pipe(autoprfxr())
        .pipe(gcmq())
        .pipe(csscomb())
        .pipe(cssmin())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest(path.css.minified))
        .pipe(bs.stream());
});

gulp.task('minify', ['minify:js', 'minify:css']);

gulp.task('make:smart-grid', () => {
    smartgrid(path.smartgrid, {
        outputStyle: 'sass',
        columns: 12,
        offset: '30px',
        mobileFirst: false,
        container: {
            maxWidth: '1200px',
            fields: '15px'
        },
        breakPoints: {
            lg: {
                width: '1200px'
            },
            md: {
                width: '991px'
            },
            sm: {
                width: '767px'
            },
            xs: {
                width: '575px'
            },
            xxs: {
                width: '320px'
            }
        }
     
    });    
});

gulp.task('dev', ['minify'], () => {
    bs.init({
        proxy: 'gnk.loc'
    });

    gulp.watch(path.js.assembled, ['minify:js']).on('change', bs.reload);
    gulp.watch(path.css.assembled, ['minify:css']);
    gulp.watch('./**/*.php').on('change', bs.reload);
});

gulp.task('watch', () => {
    gulp.watch(path.js.assembled, ['minify:js']);
    gulp.watch(path.css.assembled, ['minify:css']);
});